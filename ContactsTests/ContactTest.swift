//
//  ContactsTests.swift
//  ContactsTests
//
//  Created by Muneer KK on 31/08/19.
//  Copyright © 2019 GoJek. All rights reserved.
//

import XCTest

import XCTest
@testable import Contacts

// This test requires an active internet connection
class ContactsTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // test if http manager get func is working
    func testGetRequestAndInternet() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        NetworkManager.shared.get(urlString: "http://www.google.com", completionBlock: { (data,error)  in
            XCTAssertTrue(data != nil)
        })
    }
    
    // test if gojek api available
    func testGojekAPI() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        NetworkManager.shared.get(urlString: AppConstants.baseURL + AppConstants.ApiURL.contactURL.rawValue , completionBlock: { (data,error) in
            XCTAssertTrue(data != nil)
        })
    }
    
    

}


