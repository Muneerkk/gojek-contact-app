//
//  ViewController.swift
//  Contacts
//
//  Created by admin on 29/01/19.
//  Copyright © 2019 GoJek. All rights reserved.
//

import UIKit
private typealias ContactsTableViewMethods = ContactsListViewController

private typealias Constants = ContactsListViewController

class ContactsListViewController:BaseViewController{

    @IBOutlet weak var contactsListTableView: UITableView!
    // MARK: Variables
    var contactVM = ContactVM()
    

    var selectedContactURL: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Initiliaze UI elements
        initializeUIElemenst()
        
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Load Contacts
        loadAllContacts()
    }
    fileprivate func initializeUIElemenst() {
        initializeTableView()
        navigationItem.title = AppConstants.navTitle
    }
    fileprivate func loadAllContacts() {
        contactVM.getAllContacts {[weak self] (response,error) in
            guard let weakSelf = self else {
                return
            }
            if let errorObject = error as NSError?{
                self?.showAlert(title: AppConstants.alertTitle, message: errorObject.localizedDescription, destructiveButtonTitle: "OK")
            } else {
                
                if let contactList = response as? [Contact],let sortedContactList = self?.contactVM.fetchContactList( contactList){
                    self?.contactVM.contactList = sortedContactList
                    weakSelf.contactsListTableView.reloadData()
                }
                
            }
            
        }
        
    }
    fileprivate func initializeTableView() {
        // Register TableView Cell
        contactsListTableView.register(UINib(nibName: AppConstants.NibNames.ContactTableViewCellNib.rawValue, bundle: Bundle.main), forCellReuseIdentifier: AppConstants.TableViewCellIdentifier.ContactTableViewCellID.rawValue)
    }
    

   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewContact", let destination = segue.destination as? ContactDetailsViewController {
            destination.contactURL = selectedContactURL
        }
    }
}
extension ContactsTableViewMethods:UITableViewDataSource, UITableViewDelegate  {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactVM.contactList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.rowHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = contactVM.contactList[indexPath.row]
        
        selectedContactURL = data.url
        performSegue(withIdentifier: "viewContact", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let contactCell =  contactsListTableView.dequeueReusableCell(withIdentifier: AppConstants.TableViewCellIdentifier.ContactTableViewCellID.rawValue) as! ContactTableViewCell
        let contact = contactVM.contactList[indexPath.row]
        contactCell.configCell(contact:contact )
        
        return contactCell
    }

}
extension Constants {
    struct Constants {
        static let rowHeight:CGFloat = 56.0
    }
}



