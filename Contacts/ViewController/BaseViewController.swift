//
//  BaseViewController.swift
//  Contacts
//
//  Created by Muneer KK on 31/08/19.
//  Copyright © 2019 GoJek. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
internal typealias AlertCompletion = () -> ()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func showAlert(title: String? =  nil,
                   message: String? = nil,
                   destructiveButtonTitle: String? = nil,
                   defaultButtonTitle: String = "OK",
                   OKCompletion: @escaping AlertCompletion = {},
                   CancelCompletion: @escaping AlertCompletion = {}) {
        DispatchQueue.main.async { [weak self]() -> Void in
            let alert = UIAlertController(title: title ?? "",
                                          message: message ?? "",
                                          preferredStyle: UIAlertController.Style.alert)
            if destructiveButtonTitle != nil {
                alert.addAction(UIAlertAction(title: destructiveButtonTitle,
                                              style: UIAlertAction.Style.default,
                                              handler: { (alert) -> Void in
                                                CancelCompletion()
                }))
            }
            alert.addAction(UIAlertAction(title: defaultButtonTitle,
                                          style: UIAlertAction.Style.default,
                                          handler: { (alert) -> Void in
                                            OKCompletion()
            }))
            self?.present(alert, animated: true, completion: nil)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
