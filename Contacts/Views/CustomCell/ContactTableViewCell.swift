//
//  ContactTableViewCell.swift
//  Contacts
//
//  Created by Muneer KK on 31/08/19.
//  Copyright © 2019 GoJek. All rights reserved.
//

import UIKit
import SDWebImage
class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var contactImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        // set rounded imageview
        contactImageView.layer.cornerRadius = contactImageView.frame.size.width/2
    }
    func configCell(contact:Contact) {
        self.nameLabel.text = contact.fullName
        if let urlString = contact.profilePic, let url = URL(string: urlString){
             self.contactImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder_photo"))
        }
        favButton.isHidden = !contact.favorite!
        
       
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
