//
//  Base.swift
//  Contacts
//
//  Created by Muneer KK on 30/08/19.
//  Copyright © 2019 GoJek. All rights reserved.
//

import Foundation
class BaseVM: NSObject {
    
    internal typealias VMCompletionBlock = (_ errorObject : Error?) -> ()
    internal typealias VMDataCompletionBlock = (_ responseObject : Any?, _ errorObject : NSError?) -> ()
}
