//
//  ContactVM.swift
//  Contacts
//
//  Created by Muneer KK on 30/08/19.
//  Copyright © 2019 GoJek. All rights reserved.
//

import Foundation

class ContactVM: BaseVM {
    
    static let shared: ContactVM = ContactVM()
    var apiHandler:ContactAPIHandler    = ContactAPIHandler()
    var dataHandler:ContactDataHandler  = ContactDataHandler()
    var contactList = [Contact]()
    
    //MARK: - API
    /**
     Get list of contacts for
     */
    func getAllContacts(_ onCompletion:@escaping VMDataCompletionBlock) {
        apiHandler.getAllContacts() {
            [weak self](responseObject, errorObject) -> () in
            guard let `self` = self else {
                return
            }
            guard let responseData = responseObject else {
                DispatchQueue.main.async {
                    onCompletion(nil,errorObject)
                }
                return
            }
                self.dataHandler.fetchContactModel(responseData as! Data, completionHandler: { (response,error) in
                    DispatchQueue.main.async {
                        onCompletion(response,error)
                    }
                })
            
        }
    }
    func fetchContactList(_ contacts:[Contact])->[Contact]? {
        return contacts.sorted(by: { (first, second) -> Bool in
            first.fullName > second.fullName
        })
    }
}
