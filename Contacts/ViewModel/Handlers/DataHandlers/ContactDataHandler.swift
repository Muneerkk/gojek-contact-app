//
//  ContactDataHandler.swift
//  Contacts
//
//  Created by Muneer KK on 30/08/19.
//  Copyright © 2019 GoJek. All rights reserved.
//

import Foundation
class ContactDataHandler: BaseDataHandler {
    
    
    //MARK: - Public Methods
    
    func fetchContactModel(_ responseData:Data, completionHandler:@escaping DataHandlerCompletionBlock){
        let decoder = JSONDecoder.init()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        do {
            let response =  try decoder.decode([Contact].self, from: responseData)
            completionHandler(response,nil)
            
        } catch {
            completionHandler(nil, error as NSError)
        }
        
        
        
    }
    
    
    
}
