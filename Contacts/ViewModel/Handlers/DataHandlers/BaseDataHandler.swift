//
//  BaseDataHandler.swift
//  Contacts
//
//  Created by Muneer KK on 31/08/19.
//  Copyright © 2019 GoJek. All rights reserved.
//

import Foundation

class BaseDataHandler: NSObject {
    internal typealias DataHandlerCompletionBlock = (_ responseObject : Any?,_ errorObject : NSError?) -> ()
    
}
