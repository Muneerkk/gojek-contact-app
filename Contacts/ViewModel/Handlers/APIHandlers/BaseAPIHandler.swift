//
//  BaseAPIHandler.swift
//  Contacts
//
//  Created by Muneer KK on 30/08/19.
//  Copyright © 2019 GoJek. All rights reserved.
//

import Foundation

class BaseAPIHandler: NSObject {
    
    internal typealias ApiCompletionBlock = (_ responseObject : AnyObject?, _ errorObject : NSError?) -> ()
    internal var networkManager : NetworkManager = NetworkManager()
    
    //MARK: - initializers
    override init(){
    }
    
}
