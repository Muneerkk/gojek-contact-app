//
//  ContactAPIHandler.swift
//  Contacts
//
//  Created by Muneer KK on 30/08/19.
//  Copyright © 2019 GoJek. All rights reserved.
//

import Foundation


class ContactAPIHandler: BaseAPIHandler {
    
    /* API to fetch Contact details */
    func getAllContacts(onCompletion:@escaping ApiCompletionBlock) {
        let urlString =  networkManager.baseURL + AppConstants.ApiURL.contactURL.rawValue
        self.networkManager.get(urlString: urlString) { (responseObject, errorObject) in
             onCompletion(responseObject, errorObject)
        }
        
    }
    
}

