//
//  NetWorkManager.swift
//  Contacts
//
//  Created by Muneer KK on 30/08/19.
//  Copyright © 2019 GoJek. All rights reserved.
//

import Foundation

// Singleton used to execute HTTP request and return it's response
// currently just support GET request and simply returns the content
// without error codes or messages, returns nil in case of error

class NetworkManager {
    
    
     internal typealias ApiCompletionBlock = (_ responseObject : AnyObject?, _ errorObject : NSError?) -> ()
    // MARK: Shared Instance
    static let shared: NetworkManager = NetworkManager()
    var baseURL = AppConstants.baseURL
    
    // get request, run synchronously for threading simplicity
    public func get(urlString: String, completionBlock:@escaping ApiCompletionBlock) {
        
        let url = URL(string: urlString)
        if let usableUrl = url {
            let request = URLRequest(url: usableUrl)
            let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                completionBlock(data as AnyObject?, error as NSError?)
            })
            task.resume()
        }
    }
}
