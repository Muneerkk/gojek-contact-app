//
//  AppConstants.swift
//  Contacts
//
//  Created by Muneer KK on 30/08/19.
//  Copyright © 2019 GoJek. All rights reserved.
//

import Foundation
class AppConstants: NSObject {
    
    // Enum to change the base URL
    enum BaseURL:String {
        case production   = "https://gojek-contacts-app.herokuapp.com"
        case test         = "http://gojek-contacts-app.herokuapp.com"

    }
    
    static let baseURL    =  BaseURL.test.rawValue
    
    
    // MARK: API URLS -
    enum ApiURL:String {
        case contactURL   = "/contacts.json"
    }
    //MARK: Strings
    static let alertTitle = "Failed"
    static let navTitle   = "Contacts"
    
    // MARK: NibNames -
    enum NibNames:String {
        case ContactTableViewCellNib          = "ContactTableViewCell"
    }
    // MARK: TableViewCellIdentifier -
    enum TableViewCellIdentifier:String {
        case ContactTableViewCellID           = "ContactTableViewCellID"
    }
}
